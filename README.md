#Willis Style Guide

> index.html

This is a single html file with various heading, table, form, button, text, panel and image styles.

> style.css

This is a stylesheet with all of my custom styles

**notes:** included in the html, you will find external to jQuery, bootstrap and font awesome (which this project depends on)

**PREVIEW ONLINE:** http://brewery.maltcreative.com.au/willis-styleguide
